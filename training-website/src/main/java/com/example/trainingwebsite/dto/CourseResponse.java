package com.example.trainingwebsite.dto;

public class CourseResponse {
	
	private String courseName;
    private String subTopicName;

    public CourseResponse(String courseName, String subTopicName) {
        this.courseName = courseName;
        this.subTopicName = subTopicName;
    }

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getSubTopicName() {
		return subTopicName;
	}

	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}

}
