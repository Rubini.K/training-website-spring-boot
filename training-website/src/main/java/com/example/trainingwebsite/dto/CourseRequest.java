package com.example.trainingwebsite.dto;

import com.example.trainingwebsite.model.Course;


public class CourseRequest {
	
	private Course course;

	public Course getCourse() {
		return course;
	}

	public void setCourse(Course course) {
		this.course = course;
	}

	@Override
	public String toString() {
		return "course [course=" + course + "]";
	}
}
