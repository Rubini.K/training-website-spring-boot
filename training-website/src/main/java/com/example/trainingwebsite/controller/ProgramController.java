package com.example.trainingwebsite.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.trainingwebsite.model.ProgramModel;
import com.example.trainingwebsite.repository.ProgramRepository;

@CrossOrigin(origins = "http://localhost:8081")
@RestController
@RequestMapping("/api")
public class ProgramController {

	@Autowired
	  ProgramRepository programRepository;

	  @GetMapping("/upcommingPrograms")
	  public ResponseEntity<List<ProgramModel>> getAllUpcommingPrograms(@RequestParam(required = false) String programName) {
	   
		  try {
		      List<ProgramModel> upcommingPrograms = new ArrayList<ProgramModel>();

		      if (programName == null)
		        programRepository.findAll().forEach(upcommingPrograms::add);
		      else
		        programRepository.findByProgramName(programName).forEach(upcommingPrograms::add);

		      if (upcommingPrograms.isEmpty()) {
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		      }

		      return new ResponseEntity<>(upcommingPrograms, HttpStatus.OK);
		    } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  
	  }

	  @GetMapping("/upcommingPrograms/{id}")
	  public ResponseEntity<ProgramModel> getUpcommingProgramById(@PathVariable("id") long id) {
	    
		  Optional<ProgramModel> programData = programRepository.findById(id);

		    if (programData.isPresent()) {
		      return new ResponseEntity<>(programData.get(), HttpStatus.OK);
		    } else {
		      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		    }
		  
	  }

	  @PostMapping("/upcommingPrograms")
	  public ResponseEntity<ProgramModel> createUpcommingProgram(@RequestBody ProgramModel program) {
	    
		  try {
		      ProgramModel _program = programRepository
		          .save(new ProgramModel(program.getProgramName(), program.getProgramDescription(), program.getProgramType(), program.getProgramStartdate(), program.getProgramPrice(), false));
		      return new ResponseEntity<>(_program, HttpStatus.CREATED);
		    } catch (Exception e) {
		      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  
	  }

	  @PutMapping("/upcommingPrograms/{id}")
	  public ResponseEntity<ProgramModel> updateUpcommingProgram(@PathVariable("id") long id, @RequestBody ProgramModel program) {
	    
		  Optional<ProgramModel> programData = programRepository.findById(id);

		    if (programData.isPresent()) {
		      ProgramModel _program = programData.get();
		      _program.setProgramName(program.getProgramName());
		      _program.setProgramDescription(program.getProgramDescription());
		      _program.setProgramType(program.getProgramType());
		      _program.setProgramStartdate(program.getProgramStartdate());
		      _program.setProgramPrice(program.getProgramPrice());
		      _program.setPublished(program.isPublished());
		      return new ResponseEntity<>(programRepository.save(_program), HttpStatus.OK);
		    } else {
		      return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		    }
		  
	  }

	  @DeleteMapping("/upcommingPrograms/{id}")
	  public ResponseEntity<HttpStatus> deleteUpcommingProgram(@PathVariable("id") long id) {
	    
		  try {
		      programRepository.deleteById(id);
		      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		    } catch (Exception e) {
		      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  
	  }

	  @DeleteMapping("/upcommingPrograms")
	  public ResponseEntity<HttpStatus> deleteAllUpcommingPrograms() {
	    
		  try {
		      programRepository.deleteAll();
		      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		    } catch (Exception e) {
		      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  
	  }

	  @GetMapping("/upcommingPrograms/published")
	  public ResponseEntity<List<ProgramModel>> findByPublished() {
	    
		  try {
		      List<ProgramModel> programs = programRepository.findByPublished(true);

		      if (programs.isEmpty()) {
		        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		      }
		      return new ResponseEntity<>(programs, HttpStatus.OK);
		    } catch (Exception e) {
		      return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		    }
		  
	  }
	
}
