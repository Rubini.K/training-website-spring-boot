package com.example.trainingwebsite.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.trainingwebsite.dto.CourseRequest;
import com.example.trainingwebsite.dto.CourseResponse;
import com.example.trainingwebsite.model.Course;
import com.example.trainingwebsite.model.ProgramModel;
import com.example.trainingwebsite.repository.CourseRepository;
import com.example.trainingwebsite.repository.SubtopicRepository;


@CrossOrigin(origins = "http://localhost:8081")
@RequestMapping("/api")
@RestController
public class CourseController {
	
	 @Autowired
	 CourseRepository courseRepository;
	 @Autowired
	 SubtopicRepository subtopicRepository;
	 
	 @PostMapping("/addCourse")
	    public Course addCourse(@RequestBody CourseRequest request){
	       return courseRepository.save(request.getCourse());
	    }

	    @GetMapping("/findAllCourses")
	    public List<Course> findAllCourses(){
	        return courseRepository.findAll();
	    }

	    @GetMapping("/getInfo")
	    public List<CourseResponse> getJoinInformation(){
	        return courseRepository.getJoinInformation();
	    }

	 
	 
}
