package com.example.trainingwebsite.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "Course")
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long cid;

	@Column(name = "courseName")
	private String courseName;
	
	@Column(name = "courseCategory")
	private String courseCategory;
	
	@OneToMany(targetEntity = Subtopic.class, fetch = FetchType.LAZY,cascade = CascadeType.ALL)
    @JoinColumn(name ="cp_fk",referencedColumnName = "cid")
    private List<Subtopic> subtopics;
	
	public Course() {

	}

	public Course(String courseName, String courseCategory, List<Subtopic> subtopics) {
		this.courseName = courseName;
		this.courseCategory = courseCategory;
		this.subtopics = subtopics;
	}

	public String getCourseCategory() {
		return courseCategory;
	}

	public void setCourseCategory(String courseCategory) {
		this.courseCategory = courseCategory;
	}

	public long getCid() {
		return cid;
	}

	public void setCid(long cid) {
		this.cid = cid;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public List<Subtopic> getSubtopics() {
		return subtopics;
	}

	public void setSubtopics(List<Subtopic> subtopics) {
		this.subtopics = subtopics;
	}
	
	@Override
	public String toString() {
		return "course [cid=" + cid + ", courseName=" + courseName + ", courseCategory=" + courseCategory + ", subtopics=" + subtopics + "]";
	}

}
