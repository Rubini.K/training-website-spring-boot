package com.example.trainingwebsite.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subtopic")
public class Subtopic {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long sid;

	@Column(name = "subTopicName")
	private String subTopicName;

	public Subtopic() {

	}

	public Subtopic(String subTopicName) {
		this.subTopicName = subTopicName;
		
	}

	public long getSid() {
		return sid;
	}

	public void setSid(long sid) {
		this.sid = sid;
	}

	public String getSubTopicName() {
		return subTopicName;
	}

	public void setSubTopicName(String subTopicName) {
		this.subTopicName = subTopicName;
	}
	
	@Override
	public String toString() {
		return "subTopic [sid=" + sid + ", subTopicName=" + subTopicName + "]";
	}

	
}
