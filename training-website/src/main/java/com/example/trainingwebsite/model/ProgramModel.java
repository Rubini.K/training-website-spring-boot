package com.example.trainingwebsite.model;

import javax.persistence.*;

@Entity
@Table(name = "upcommingPrograms")
public class ProgramModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@Column(name = "programName")
	private String programName;

	@Column(name = "programDescription")
	private String programDescription;
	
	@Column(name = "programType")
	private String programType;

	@Column(name = "programStartdate")
	private String programStartdate;
	
	@Column(name = "programPrice")
	private String programPrice;
	
	@Column(name = "published")
	private boolean published;

	public ProgramModel() {

	}

	public ProgramModel(String programName, String programDescription, String programType, String programStartdate, String programPrice, boolean published) {
		this.programName = programName;
		this.programDescription = programDescription;
		this.programType = programType;
		this.programStartdate = programStartdate;
		this.programPrice = programPrice;
		this.published = published;
	}

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getProgramName() {
		return programName;
	}

	public void setProgramName(String programName) {
		this.programName = programName;
	}

	public String getProgramDescription() {
		return programDescription;
	}

	public void setProgramDescription(String programDescription) {
		this.programDescription = programDescription;
	}

	public String getProgramType() {
		return programType;
	}

	public void setProgramType(String programType) {
		this.programType = programType;
	}

	public String getProgramStartdate() {
		return programStartdate;
	}

	public void setProgramStartdate(String programStartdate) {
		this.programStartdate = programStartdate;
	}

	public String getProgramPrice() {
		return programPrice;
	}

	public void setProgramPrice(String programPrice) {
		this.programPrice = programPrice;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	@Override
	public String toString() {
		return "UpcommingPrograms [id=" + id + ", programName=" + programName + ", programDescription=" + programDescription + ", programType=" + programType + ", programStartdate=" + programStartdate + ", programPrice=" + programPrice + ", published=" + published + "]";
	}

}
