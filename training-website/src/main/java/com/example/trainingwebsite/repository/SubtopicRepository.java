package com.example.trainingwebsite.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.trainingwebsite.model.Subtopic;

public interface SubtopicRepository extends JpaRepository<Subtopic,Long>{

}
