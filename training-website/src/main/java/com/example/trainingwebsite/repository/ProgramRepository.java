package com.example.trainingwebsite.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import com.example.trainingwebsite.model.ProgramModel;

public interface ProgramRepository extends JpaRepository<ProgramModel, Long> {
	 List<ProgramModel> findByPublished(boolean published);
	 List<ProgramModel> findByProgramName(String programName);
}
