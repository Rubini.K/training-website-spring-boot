package com.example.trainingwebsite.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.trainingwebsite.dto.CourseResponse;
import com.example.trainingwebsite.model.Course;


public interface CourseRepository extends JpaRepository<Course, Long>{
	
	@Query("SELECT new com.example.trainingwebsite.dto.CourseResponse(c.courseName , s.subTopicName) FROM Course c JOIN c.subtopics s")
    public List<CourseResponse> getJoinInformation();

}
